import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleCreateComponent } from './sale-create/sale-create.component';
import { SalesRoutingModule } from './sales-routing.module';



@NgModule({
  declarations: [SaleCreateComponent],
  imports: [
    CommonModule, 
    SalesRoutingModule
  ]
})
export class SalesModule { }
