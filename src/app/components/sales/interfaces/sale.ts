import { Bike } from '../../bikes/interfaces/bike';
import { Client } from '../../clients/interfaces/clients';


export interface ISale {
    id?: number,
    date?: Date,
    clientId?: number,
    clientName?: string,
    bikeId?: number,
    bikeSerial?: string,
    client?: Client,
    bike?: Bike
}