import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BokesViewComponent } from './bokes-view.component';

describe('BokesViewComponent', () => {
  let component: BokesViewComponent;
  let fixture: ComponentFixture<BokesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BokesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BokesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
