import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { Bike } from '../interfaces/bike';
@Component({
  selector: 'app-bikes-create',
  templateUrl: './bikes-create.component.html',
  styleUrls: ['./bikes-create.component.styl']
})
export class BikesCreateComponent implements OnInit {

  bikeFormGroup: FormGroup;
  bike: Bike;

  statusSearchSerial: boolean = true;

  formSearchBike = this.formBuilder.group({
    serial: ['']
  });
  

  constructor(private formBuilder: FormBuilder, private bikesService: BikesService) {
    this.bikeFormGroup = this.formBuilder.group({
      model:['', Validators.compose([
        Validators.required,
        Validators.maxLength(4)
      ])],
      price:['', Validators.required],
      
      serial:['', Validators.compose([
        Validators.required,
        Validators.maxLength(5)
      ])],
    });
   }

  ngOnInit() {
  }

  saveBike(){
    console.log('Datos', this.bikeFormGroup.value);
    this.bikesService.saveBike(this.bikeFormGroup.value)
    .subscribe(res => {
      console.log('SAVE OK', res);
    }, error => {
      console.error('Error', error);
    });

  }

  searchBike() {
    console.warn('Data', this.formSearchBike.value);
    this.bikesService.getBikeBySerial(this.formSearchBike.value.serial)
    .subscribe(res => {
      console.warn('Response bike by serial', res);
      this.bike = res;
      this.statusSearchSerial = true;
    }, error => {
      console.warn('No encontrado', error);
      this.bike = null;
      this.statusSearchSerial = false;
    });
  }

}
