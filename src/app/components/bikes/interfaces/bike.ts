export interface Bike {
    id: number;
    model: string;
    price: number;
    serial: string;
    image: string;
    imageContentType: string;
}