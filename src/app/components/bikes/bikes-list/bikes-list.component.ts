import { Component, OnInit } from '@angular/core';
import { Bike } from '../interfaces/bike';
import { BikesService } from '../bikes.service';


@Component({
  selector: 'app-bikes-list',
  templateUrl: './bikes-list.component.html',
  styleUrls: ['./bikes-list.component.styl']
})
export class BikesListComponent implements OnInit {

  
  public bikesList: Bike [];
  constructor(private bikesServer: BikesService) { }

  ngOnInit() {
    this.bikesServer.query()
    .subscribe(res => {
      this.bikesList = res;
      console.log('Respose Data',res)
    },
    error => console.error('Error', error)
    );
  }

  deleteItem(id: string){
    console.warn('ID', id);
    this.bikesServer.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deletec ok..');
      this.ngOnInit();
    }, error => console.warn('Error', error));
  }

}
