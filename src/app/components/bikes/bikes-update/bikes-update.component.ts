import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Bike } from '../interfaces/bike';

@Component({
  selector: 'app-bikes-update',
  templateUrl: './bikes-update.component.html',
  styleUrls: ['./bikes-update.component.styl']
})

export class BikesUpdateComponent implements OnInit {

  bikeFormGroup: FormGroup

  constructor(
    private formBuilder: FormBuilder, 
    private bikesService: BikesService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) {
    this.bikeFormGroup = this.formBuilder.group({
      id:[''],
      model:['', Validators.compose([
        Validators.required,
        Validators.maxLength(4)
      ])],
      price:[''],
      
      serial:[''],
    });
   }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log('ID PATH', id);
    this.bikesService.getBikeById(id)
    .subscribe(res => {
      console.log('get data ok', res);
      this.loadForm(res);
    });
  }

  saveBike(){
    console.log('Datos', this.bikeFormGroup.value);
    this.bikesService.updateBike(this.bikeFormGroup.value)
    .subscribe(res => {
      console.log('SAVE OK', res);
      this.router.navigate(['../bikes-list'])
    }, error => {
      console.error('Error', error);
    });

  }

  private loadForm(bike: Bike){
    this.bikeFormGroup.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial:bike.serial
    })
  }

}
