import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClientsService } from '../clients.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from '../interfaces/clients';

@Component({
  selector: 'app-clients-update',
  templateUrl: './clients-update.component.html',
  styleUrls: ['./clients-update.component.styl']
})
export class ClientsUpdateComponent implements OnInit {

  clientFormGroup: FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private clientsService: ClientsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.clientFormGroup = this.formBuilder.group({
      id:[''],
      name:[''],
      document:[''],
      email:[''],
      phoneNumber:[''],
      documentType:[''],
    });
   }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('Id path', id);
    this.clientsService.getClientById(id)
    .subscribe(res => {
      console.warn('Get data ok', res);
      this.loadForm(res);
    });
  }

  saveClient(){
    console.log('Datos', this.clientFormGroup.value);
    this.clientsService.updateClient(this.clientFormGroup.value)
    .subscribe(res => {
      console.warn('SAVE OK', res);
      this.router.navigate(['/clients/clients-list'])
    }, error => {
      console.warn('Error', error);
    });

  }

  private loadForm(client: Client){
    this.clientFormGroup.patchValue({
      id: client.id,
      name: client.name,
      document: client.document,
      email:client.email,
      phoneNumber: client.phoneNumber,
      documentType: client.documentType
    })
  }

}
