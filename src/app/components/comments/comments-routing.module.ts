import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentsCreateComponent } from './comments-create/comments-create.component';
import { CommentsListComponent } from './comments-list/comments-list.component';


const routes: Routes = [
  {
    path: 'comments-create',
    component: CommentsCreateComponent
  },
  {
    path: 'comments-list',
    component: CommentsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentsRoutingModule { }
