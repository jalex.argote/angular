import { Component, OnInit } from '@angular/core';
import { IComments } from '../interfaces/comments';
import { CommentsService } from '../comments.service';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.styl']
})
export class CommentsListComponent implements OnInit {

  public commentsList: IComments [];

  constructor( private commentsService: CommentsService) { }

  ngOnInit() {
    this.commentsService.query()
   .subscribe(res => {
     this.commentsList = res;
   });
  }

}
