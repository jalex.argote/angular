import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsCreateComponent } from './posts-create/posts-create.component';
import { PostsListComponent } from './posts-list/posts-list.component';


const routes: Routes = [
  {
    path: 'posts-create',
    component: PostsCreateComponent
  },
  {
    path: 'posts-list',
    component: PostsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
