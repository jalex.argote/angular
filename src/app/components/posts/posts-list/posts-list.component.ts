import { Component, OnInit } from '@angular/core';
import { IPosts } from '../interfaces/posts';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.styl']
})
export class PostsListComponent implements OnInit {
  
  public postsList: IPosts [];

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.query()
    .subscribe(res => {
      this.postsList = res;
    });
  }

}
