import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IPosts } from '../interfaces/posts';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts-create',
  templateUrl: './posts-create.component.html',
  styleUrls: ['./posts-create.component.styl']
})
export class PostsCreateComponent implements OnInit {
  postsFormGroup: FormGroup;
  posts: IPosts[];

  statusSearchUserId: boolean = true;
  formSearchPosts= this.formBuilder.group({
    userId: ['']
  });

  constructor(private formBuilder: FormBuilder, private postsService: PostsService) { }

  ngOnInit() {
  }

  searchPosts() {
    console.warn('Data', this.formSearchPosts.value);
    this.postsService.getPostsByUserId(this.formSearchPosts.value.userId)
    .subscribe(res => {   
      if(res.length > 0){
        this.posts = res;
        this.statusSearchUserId = true;
        console.warn('Response posts by user Id', res);
      }else{
        console.warn('No encontrado', res);
        this.statusSearchUserId = false;
        this.posts = [];
      }
      
    });
    /*, error => {
      console.warn('No encontrado', error);
      this.posts = null;
      */
  
  }


}
