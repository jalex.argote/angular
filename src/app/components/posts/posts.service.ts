import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPosts } from './interfaces/posts';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IPosts[]> {
    return this.http.get<IPosts[]>(`${environment.END_POINT_JSON}/posts`)
    .pipe(map(res => {
      return res;
    }))
  }

  public savePosts(posts: IPosts): Observable<IPosts> {
    return this.http.post<IPosts>(`${environment.END_POINT_JSON}/posts`, posts)
    .pipe(map(res => {
      return res;
    }));
  } 

  public getPostsByUserId(userId: string): Observable<IPosts[]>{
    let params = new HttpParams();
    params = params.append('userId', userId);
    console.warn('Params', params);
    return this.http.get<IPosts[]>(`${environment.END_POINT_JSON}/posts`, {params: params})
      .pipe(map(res => {
        return res;
      }));
  }

}
