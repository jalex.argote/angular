import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'bikes',
    loadChildren:() => import('./components/bikes/bikes.module')
    .then(modulo => modulo.BikesModule)
  },
  {
    path: 'admin',
    loadChildren:() => import('./components/admin/admin.module')
    .then(modulo => modulo.AdminModule)
  },
  {
    path: 'clients',
    loadChildren:() => import('./components/clients/clients.module')
    .then(modulo => modulo.ClientsModule)
  },
  {
    path: 'sales',
    loadChildren:() => import('./components/sales/sales.module')
    .then(modulo => modulo.SalesModule)
  },
  {
    path: 'comments',
    loadChildren: () => import('./components/comments/comments.module')
    .then(modulo => modulo.CommentsModule)
  },
  {
    path: 'posts',
    loadChildren: () => import('./components/posts/posts.module')
    .then(modulo => modulo.PostsModule)
  } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
